echo "debut provisioning"  
dnf -y update
dnf module install -y python36
dnf install -y epel-release
dnf info ansible --repo=epel
dnf install -y ansible python3-argcomplete
dnf install -y vim code
dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
dnf install docker-ce --nobest -y
systemctl start docker
systemctl enable docker
#Création du fichier Dockerfile
touch /home/vagrant/Dockerfile
echo "FROM debian:9 
EXPOSE 22/tcp
EXPOSE 80/tcp
RUN apt-get update -y \\
&& apt-get install -y nginx" >> /home/vagrant/Dockerfile 
