FROM debian:9

RUN apt-get update -yq \
&& apt-get install  openssh-server -y \
&& apt-get install nginx -y \
&& apt-get install vim -y

WORKDIR /tmp

EXPOSE 80
EXPOSE 22

CMD service nginx start
